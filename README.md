# Nội Thất Nhật Quân

Nội thất Nhật Quân chuyên cung cấp các đồ gỗ nội thất từ bàn ghế phòng khách, giường ngủ, bàn ăn... với đa dạng thiết kế đi kèm là chất lượng cao cấp. Dịch vụ nội thất Nhật Quân đảm bảo đem đến khách hàng những sản phẩm tốt nhất trên thị trường

- Địa chỉ: Ngõ mới, Châu Phong, Liên Hà, Đông Anh, Hà Nội

- SDT: 0393466686

Nội thất Nhật Quân là 1 trong các nhà xưởng lâu đời còn còn đó đến giờ ở làng nghề Châu Phong, Liên Hà, Đông Anh, Hà Nội. Xưởng phân phối khởi đầu giai đoạn hình thành từ những năm 2000, đã có hơn 20 năm kinh nghiệm trong việc sản xuất những sản phẩm nội thất, phân phối cho đa số quý khách sinh sống tại các khu vực phía Bắc Việt Nam.

Xưởng phân phối mang đội ngũ nghệ nhân lành nghề, cả đời gắn bó sở hữu nghề mộc, với bàn tay điêu luyện, cho ra mắt các sản phẩm đồ gỗ hoa văn đẹp, mượt mà, phù hợp với thị hiếu và thiên hướng của thị trường.

https://noithatnhatquan.com/

https://www.flickr.com/people/197893351@N04/

https://vi.gravatar.com/noithatnhatquan

https://www.tumblr.com/noithatnhatquan
